const cateringController = (function() {

    const cateringItems = {
        MENU1: {
            name: 'MENU1',
            price: 180,
            perPerson: true,
            total: 10800,
        },
        MENU2: {
            name: 'MENU2',
            price: 50,
            perPerson: true,
            total: 0,
        },
        MENU3: {
            name: 'MENU3',
            price: 100,
            perPerson: true,
            total: 0,
        },
        STOLWIEJSKI: {
            name: 'STOLWIEJSKI',
            price: 1000,
            perPerson: true,
            total: 0,
        },
        FONTANNA1: {
            name: 'FONTANNA1',
            price: 600,
            perPerson: true,
            total: 0,
        },
        FONTANNA2: {
            name: 'FONTANNA2',
            price: 800,
            perPerson: true,
            total: 0,
        },
        FONTANNA3: {
            name: 'FONTANNA3',
            price: 1000,
            perPerson: true,
            total: 0,
        },
        KORKOWE: {
            name: 'KORKOWE',
            price: 10,
            perPerson: true,
            total: 0,
        },
        OPENBAR: {
            name: 'OPENBAR',
            price: 65,
            perPerson: true,
            total: 0,
        },
        PLONACA_MISA: {
            name: 'PLONACA_MISA',
            price: 200,
            perPerson: false,
            quantity: 1,
            total: 0,
        },
        DEKORACJE_SALI_1: {
            name: 'DEKORACJE_SALI_1',
            price: 25,
            perPerson: true,
            total: 0,
        },
        DEKORACJE_SALI_2: {
            name: 'DEKORACJE_SALI_2',
            price: 400,
            perPerson: false,
            quantity: 1,
            total: 0,
        },
        STOL_PANSTWA_MLODYCH_2: {
            name: 'STOL_PANSTWA_MLODYCH_2',
            price: 200,
            perPerson: false,
            quantity: 1,
            total: 0,
        },
        WIAZANKA_SLUBNA: {
            name: 'WIAZANKA_SLUBNA',
            price: 50,
            perPerson: false,
            quantity: 1,
            total: 0,
        },
        DODATKI_DRUHNA: {
            name: 'DODATKI_DRUHNA',
            price: 100,
            perPerson: false,
            quantity: 1,
            total: 0,
        },
        DODATKI_DRUHNA_2: {
            name: 'DODATKI_DRUHNA_2',
            price: 100,
            perPerson: false,
            quantity: 1,
            total: 0,
        },
        DEKORACJE_AUTA: {
            name: 'DEKORACJE_AUTA',
            price: 500,
            perPerson: false,
            quantity: 1,
            total: 0,
        },
        DEKORACJE_AUTA_2: {
            name: 'DEKORACJE_AUTA_2',
            price: 750,
            perPerson: false,
            quantity: 1,
            total: 0,
        },
        FAJERWERKI: {
            name: 'FAJERWERKI',
            price: 1200,
            perPerson: false,
            quantity: 1,
            total: 0,
        },
        PODZIEKOWANIA_1: {
            name: 'PODZIEKOWANIA_1',
            price: 800,
            perPerson: false,
            quantity: 1,
            total: 0,
        },
        PODZIEKOWANIA_2: {
            name: 'PODZIEKOWANIA_2',
            price: 1000,
            perPerson: false,
            quantity: 1,
            total: 0,
        },
        ZUBROWKA: {
            name: 'ZUBROWKA',
            price: 26,
            perPerson: false,
            quantity: 1,
            total: 0,
        },
        WYBOROWA: {
            name: 'WYBOROWA',
            price: 29,
            perPerson: false,
            quantity: 1,
            total: 0,
        },
        STUMBRAS: {
            name: 'STUMBRAS',
            price: 35,
            perPerson: false,
            quantity: 1,
            total: 0,
        },
        FINLANDIA: {
            name: 'FINLANDIA',
            price: 39,
            perPerson: false,
            quantity: 1,
            total: 0,
        },
        WINO: {
            name: 'WINO',
            price: 23,
            perPerson: false,
            quantity: 1,
            total: 0,
        },


    };

    const INIT_STATE = {
        date: '',
        adultPersons: 60,
        childPersons: 0,
        pricePerPerson: 180,
        totalPrice: 10800,
        summary: [cateringItems['MENU1']],
    };


    let data = INIT_STATE;

    const getData = () => {
        
        const localData = localStorage.getItem('weddings');
        const continueOrder = sessionStorage.getItem('weddings_continue');

        if(sessionStorage.getItem('weddings_continue') || confirm('Kontynuować poprzednie zamówienie?')) {
            try {

                sessionStorage.setItem('weddings_continue', true);
                const storageItem = localStorage.getItem('weddings');
    
                if(storageItem === null) {
                    data = INIT_STATE;
                } else {
                    data = JSON.parse(storageItem);
                }
            } catch(e) {
                console.log(e);
            }
        } else {
            localStorage.removeItem('weddings');
            sessionStorage.setItem('weddings_continue', true);

            data = INIT_STATE;
        }   


    }

    const saveData = () => {
        try {
            localStorage.setItem('weddings', JSON.stringify(data));
        } catch (error) {
            console.log(error);
        }
    }

    const calculateItemTotal = () => {
        data.summary.forEach(item => {

            if (item.perPerson) {

                const totalSum = (data.adultPersons * item.price) + (data.childPersons * (0.4 * item.price));

                item.total = totalSum;

            } else {
                const totalSum = item.quantity * item.price;

                item.total = totalSum;
            }

        });

        saveData();
    }

    const calculateOrderTotal = () => {
        let sum = 0;

        data.summary.forEach(item => {

            sum += item.total;

        });

        data.totalPrice = sum;

        saveData();
    }

    const calculatePerPersonCost = () => {

        data.pricePerPerson = Math.round(data.totalPrice / (data.adultPersons + data.childPersons));

    }
    
    const addItem = (itemIndex) => {

        if (cateringItems[itemIndex]) {

            data.summary.push(cateringItems[itemIndex]);

            saveData();

        } else {
            console.log('Taka pozycja nie istnieje');
        }

    };

    const removeItem = (itemIndex) => {

        data.summary = data.summary.filter(item => {
            return item.name !== itemIndex;
        });

    };

    getData();

    return {

        toggleItem: (itemId) => {

            const cateringItem = cateringItems[itemId];

            const cateringItemName = cateringItem.name;

            if(cateringItemName !== undefined) {

                let isExist = false;

                data.summary.forEach(item => {
                    
                    if (item.name === cateringItemName) {

                        isExist = true;

                    }

                });

                if(!isExist) {
                    addItem(itemId);
                } else {
                    removeItem(itemId);
                }

                calculateItemTotal();
                calculateOrderTotal();
                calculatePerPersonCost();

            }

        },

        changeAdultPersonsNumber: (number) => {

            const numberOfPeople = parseInt(number);

            data.adultPersons = numberOfPeople;


            calculateItemTotal();
            calculateOrderTotal();
            calculatePerPersonCost();
            saveData();

        },

        changeChildPersonsNumber: (number) => {

            const numberOfPeople = parseInt(number);

            data.childPersons = numberOfPeople;

            calculateItemTotal();
            calculateOrderTotal();
            calculatePerPersonCost();
            saveData();

        },

        changeDate: (date) => {
            const newDate = date;
            data.date = newDate;

            saveData();
        },

        addAlcohol: (id, quantity) => {

            if(cateringItems[id]) {

                cateringItems[id].quantity = quantity;

                data.summary.push(cateringItems[id]);
                calculateItemTotal();
                calculateOrderTotal();
                saveData();

            }

        },

        getDate: () => data.date,
        getTotalCosts: () => data.totalPrice,
        getChildNumber: () => data.childPersons,
        getAdultNumber: () => data.adultPersons,
        getTotalCost: () => data.totalPrice,
        getPerPersonCost: () => data.pricePerPerson,
        getSummary: () => data.summary,
        getData: () => data,

        test: () => {
            console.log(data);
        }
    }
})();

const uiController = (function() {
    const DOM = {

        navigationTrigger: document.getElementById('navigationTrigger'),
        navigationList: document.getElementById('navigation'),
        menuList: document.querySelector('.grid--menu'),
        numberOfAdultInput: document.getElementById('numberOfAdult'),
        numberOfChildInput: document.getElementById('numberOfChild'),
        date: document.getElementById('date'),
        alcoholList: document.querySelector('.pricing'),
        totalCost: document.getElementById('cost'),
        costPerPerson: document.getElementById('costPerPerson'),
        summaryList: document.querySelector('.summary__list'),
        openModal: document.getElementById('openModal'),
        closeModal: document.getElementById('closeModal'),
        modal: document.querySelector('.modal'),
        summaryValue: document.getElementById('summaryValue'),
        dateSummary: document.getElementById('dateSummary'),
        persons: document.getElementById('persons'),
        childs: document.getElementById('childs'),
        total: document.getElementById('total'),
        perperson: document.getElementById('perperson'),
        summary: document.getElementById('summary'),
    }

    return {
        getDOMElements: () => DOM,
        setDate: date => {
            DOM.date.value = date;
        },
        setChildNumber: number => {
            DOM.numberOfChildInput.value = number;
        },
        setAdultNumber: number => {
            DOM.numberOfAdultInput.value = number;
        },
        setTotalCost: number => {
            DOM.totalCost.textContent = number;
        },
        setCostPerPerson: number => {
            DOM.costPerPerson.textContent = number;
        },
        setButtonsColor: summary => {

            const allBtns = document.querySelectorAll('.btn--add');

            summary.forEach(summaryItem => {

                allBtns.forEach(item => {

                    if (summaryItem.name === item.dataset.id) {
                        item.style.background = '#333333';
                    }

                })

            });
        },
        renderSummary: summary => {

            DOM.summaryList.innerHTML = '';

            let newHTML = `<li class='bold'>
                            <p>Nazwa</p>
                            <p>Cena jednostkowa</p>
                            <p>Suma</p>
                            </li>`;

            summary.forEach(item => {

                newHTML += `<li>
                                <p>${item.name}</p>
                                <p>${item.price}</p>
                                <p>${item.total}</p>
                                <button class='btn btn--delete' data-id='${item.name}'>x</button>
                            </li>`

            });

            DOM.summaryList.insertAdjacentHTML('beforeend', newHTML);

        }

    }
})();

const appController = (function(cateringCtrl, uiCtrl) {

    const DOMElements = uiCtrl.getDOMElements();


    const updateUI = () => {

        uiCtrl.setDate(cateringCtrl.getDate());
        uiCtrl.setChildNumber(cateringCtrl.getChildNumber());
        uiCtrl.setAdultNumber(cateringController.getAdultNumber());
        uiCtrl.setTotalCost(cateringCtrl.getTotalCost());
        uiCtrl.setCostPerPerson(cateringCtrl.getPerPersonCost());
        uiCtrl.setButtonsColor(cateringCtrl.getSummary());
        uiCtrl.renderSummary(cateringCtrl.getSummary());
        
    }


    const setupEventListeners = () => {

        // 1. Navigation listener

        

        // 2. Toggle item

        DOMElements.menuList.addEventListener('click', event => {

            // Event delegation

            if (event.target.className === 'btn btn--add') {

                const id = event.target.dataset.id;

                cateringCtrl.toggleItem(id);

                if (event.target.style.background === '' || event.target.style.background === 'transparent') {

                    event.target.style.background = '#333333';

                } else {

                    event.target.style.background = 'transparent';

                }

                updateUI();

            }

        });

        // 3. Change number of adult people

        DOMElements.numberOfAdultInput.addEventListener('blur', event => {

            cateringCtrl.changeAdultPersonsNumber(event.target.value);
            updateUI();

        });

        // 4. Change number of child people

        DOMElements.numberOfChildInput.addEventListener('blur', event => {

            cateringCtrl.changeChildPersonsNumber(event.target.value);
            updateUI();

        });

        // 5. Change date

        DOMElements.date.addEventListener('blur', event => {

            cateringCtrl.changeDate(event.target.value);
            updateUI();

        });

        // 6. Get alcohol bootles

        DOMElements.alcoholList.addEventListener('change', event => {

            const id = event.target.dataset.id;
            const quantity = parseInt(event.target.value);

            cateringCtrl.addAlcohol(id, quantity);

            updateUI();

        });

        // 7. Delete from summary

        DOMElements.summaryList.addEventListener('click', event => {
            if(event.target.className === 'btn btn--delete') {
                
                cateringCtrl.toggleItem(event.target.dataset.id);

                updateUI();

            }
        });

        // 8. Post order 

        DOMElements.openModal.addEventListener('click', () => {

            const data = cateringCtrl.getData();

            console.log(data.date);

            DOMElements.modal.classList.toggle('modal--is-open');
            DOMElements.summary.value = JSON.stringify(data.summary);
            DOMElements.dateSummary.value = data.date;
            DOMElements.persons.value = data.adultPersons;
            DOMElements.childs.value = data.childPersons;
            DOMElements.total.value = data.totalPrice;
            DOMElements.perperson.value = data.pricePerPerson;

        });

        DOMElements.closeModal.addEventListener('click',  () => {
            DOMElements.modal.classList.toggle('modal--is-open');
        });

    }

    return {
        init: () => {
            updateUI();
            setupEventListeners();
        }
    }

})(cateringController, uiController);


// Init app
document.getElementById('navigationTrigger').addEventListener('click', () => {

    document.getElementById('navigation').classList.toggle('navigation--is-open');

});

document.querySelector('.summary-trigger').addEventListener('click', () => {
    document.querySelector('.summary').classList.toggle('summary--is-open');
});

document.addEventListener('DOMContentLoaded', function() {

    appController.init();

});
